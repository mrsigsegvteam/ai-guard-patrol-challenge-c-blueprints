// Fill out your copyright notice in the Description page of Project Settings.

#include "AIGuard.h"

#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "FPSGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

// Sets default values
AAIGuard::AAIGuard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	PawnSensingComp->OnSeePawn.AddDynamic(this, &AAIGuard::OnPawnSeen);
	PawnSensingComp->OnHearNoise.AddDynamic(this, &AAIGuard::OnNoiseHeard);

	GuardState = EAIState::Idle;

	ActualIteration = 0;
	bGameEnded = false;
}

// Called when the game starts or when spawned
void AAIGuard::BeginPlay()
{
	Super::BeginPlay();

	OriginalRotaiton = GetActorRotation();

	if (TargetClass)
	{
		UGameplayStatics::GetAllActorsOfClass(this, TargetClass, PatrolPoints);
		GetWorldTimerManager().SetTimer(TimeHandle_DoIteration, this, &AAIGuard::ExecuteIteration, 3.0f);
	}
	
}

void AAIGuard::OnPawnSeen(APawn * SeenPawn)
{
	if (SeenPawn == nullptr)
	{
		return;
	}

	DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Red, false, 10.0f);

	AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		GM->CompleteMission(SeenPawn, false);
	}

	SetGuardState(EAIState::Alerted);
	bGameEnded = true;
}

void AAIGuard::OnNoiseHeard(APawn * NoiseInstigator, const FVector & Location, float Volumne)
{
	if (GuardState == EAIState::Alerted)
	{
		return;
	}

	DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Green, false, 10.0f);

	FVector Direction = Location - GetActorLocation();
	Direction.Normalize();

	FRotator NewLookAt = FRotationMatrix::MakeFromX(Direction).Rotator();
	NewLookAt.Pitch = 0.0f;
	NewLookAt.Roll = 0.0f;

	SetActorRotation(NewLookAt);

	GetWorldTimerManager().ClearTimer(TimeHandle_ResetOrientation);
	GetWorldTimerManager().SetTimer(TimeHandle_ResetOrientation, this, &AAIGuard::ResetOrientation, 3.0f);

	SetGuardState(EAIState::Suspicious);
}

void AAIGuard::ResetOrientation()
{
	if (GuardState == EAIState::Alerted)
	{
		return;
	}
	SetActorRotation(OriginalRotaiton);
	SetGuardState(EAIState::Idle);
}

void AAIGuard::SetGuardState(EAIState NewState)
{
	if (GuardState == NewState)
	{
		return;
	}

	GuardState = NewState;

	if (NewState == EAIState::Alerted || NewState == EAIState::Suspicious)
	{
		GetController()->StopMovement();
		
	}

	OnstateChanged(NewState);
}

void AAIGuard::ExecuteIteration()
{
	if (bGameEnded)
	{
		UE_LOG(LogTemp, Warning, TEXT("Last Iteration"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("Iteration"));

	if (PatrolPoints.Num() > 0)
	{

		UAIBlueprintHelperLibrary::SimpleMoveToLocation(GetController(), PatrolPoints[ActualIteration]->GetActorLocation());

		if (PatrolPoints.Num() == (ActualIteration + 1))
		{
			ActualIteration = 0;

		}
		else
		{
			ActualIteration += 1;
		}

		GetWorldTimerManager().ClearTimer(TimeHandle_DoIteration);
		GetWorldTimerManager().SetTimer(TimeHandle_DoIteration, this, &AAIGuard::ExecuteIteration, 3.0f);
	}
}

// Called every frame
void AAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

