// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AIGuard.generated.h"

class UPawnSensingComponent;

UENUM(BlueprintType)
enum class EAIState : uint8
{
	Idle,
	Suspicious,
	Alerted
};

UCLASS()
class FPSGAME_API AAIGuard : public ACharacter
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAIGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UPawnSensingComponent* PawnSensingComp;

	UFUNCTION()
	void OnPawnSeen(APawn* SeenPawn);

	UFUNCTION()
	void OnNoiseHeard(APawn* NoiseInstigator, const FVector& Location, float Volumne);

	FRotator OriginalRotaiton;

	UFUNCTION()
	void ResetOrientation();

	FTimerHandle TimeHandle_ResetOrientation;

	EAIState GuardState;
	void SetGuardState(EAIState NewState);

	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnstateChanged(EAIState NewState);

	// Guards patrol
	UPROPERTY(EditDefaultsOnly, Category = "TargetClass")
	TSubclassOf<AActor> TargetClass;
	TArray<AActor*> PatrolPoints;

	// Recursive iteration
	int ActualIteration;
	FTimerHandle TimeHandle_DoIteration;
	UFUNCTION()
	void ExecuteIteration();

	bool bGameEnded;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
